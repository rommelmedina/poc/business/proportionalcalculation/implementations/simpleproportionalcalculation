/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.simple;

import com.resolve.proportional.ProportionalCalculation;
import com.resolve.proportional.ProportionalCalculationData;
import com.resolve.proportional.models.ProportionalCalculationItem;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Proportional calculation service.
 *
 * @author Rommel Medina
 *
 */
@Service
public class SimpleProportionalCalculation implements ProportionalCalculation {
  
  /**
   * Calculation data.
   */
  @Autowired
  private ProportionalCalculationData data;
  
  @Value("${proportional.simple.error.message.invalidBonusPercentageMessage}")
  private String invalidBonusPercentageMessage;
  
  @Value("${proportional.simple.error.message.invalidProductionGoalsMessage}")
  private String invalidProductionGoalsMessage;

  @Override
  public List<ProportionalCalculationItem> compute(
      String id,
      List<ProportionalCalculationItem> items) {
    for (ProportionalCalculationItem pci : items) {
      double pcResult = this.calculate(
          data.getDefinition(id).getCategories().getCategories(),
          data.getCategoryDefinitions(pci),
          data.getDefinition(id).getPercentages().getPercentages(),
          pci.getProductionQuantity(),
          pci.getFixedBase(),
          pci.getBonus());
      pci.setResult(pcResult);
    }
    return items;
  }
  
  double calculate(
      Set<String> goalProductionCategories,
      HashMap<String, Double> productionGoals,
      HashMap<String, Double> bonusPercentages,
      HashMap<String, Double> productionQuantity,
      double fixedSalaryAmount,
      double bonusAmount) {
    this.validateproductionGoals(productionGoals);
    this.validateBonusPercentages(new ArrayList<Double>(bonusPercentages.values()));
    
    this.cutOverProduction(
        goalProductionCategories,
        productionGoals,
        productionQuantity);
    
    HashMap<String, Double> achievedGoalPercentages = this.getAchievedGoalPercentages(
        goalProductionCategories,
        productionGoals,
        productionQuantity);
    
    List<Double> achievedBonusPercentages = this.getBonusPercentagesFromProductionPortion(
        goalProductionCategories,
        bonusPercentages,
        achievedGoalPercentages);
    
    double bonus = this.getBonus(bonusAmount, achievedBonusPercentages);
    
    double salary = fixedSalaryAmount + bonus;
    
    BigDecimal bd = new BigDecimal(salary).setScale(2, RoundingMode.HALF_DOWN);
    return bd.doubleValue();
  }
  
  double getBonus(
      double bonusAmount,
      List<Double> achievedBonusPercentages) {
    double bonus = 0;
    double sum = achievedBonusPercentages.stream().mapToDouble(Double::doubleValue).sum();
    bonus = bonusAmount * (sum);
    return bonus;
  }
  
  double getBonus(
      double bonusAmount,
      double achievedProportionalCategoryProduction,
      double achievedProportionalGroupProduction) {
    double bonus = bonusAmount
        * (achievedProportionalCategoryProduction
            + achievedProportionalGroupProduction);
    return bonus;
  }
  
  void cutOverProduction(
      Set<String> goalProductionCategories,
      HashMap<String, Double> productionGoals,
      HashMap<String, Double> productionQuantities) {
    for (String category : goalProductionCategories) {
      if (productionQuantities.get(category) > productionGoals.get(category)) {
        productionQuantities.put(category, productionGoals.get(category));
      }
    }
  }
  
  double cutOverProduction(double productionGoals, double production) {
    if (production > productionGoals) {
      production = productionGoals;
    }
    return production;
  }
  
  HashMap<String, Double> getAchievedGoalPercentages(
      Set<String> goalProductionCategories,
      HashMap<String, Double> productionGoals,
      HashMap<String, Double> productionQuantitys) {
    HashMap<String, Double> achieved = new HashMap<String, Double>();
    
    for (String category : goalProductionCategories) {
      Double achievedItem = productionQuantitys.get(category) / productionGoals.get(category);
      achieved.put(category, achievedItem);
    }
    
    return achieved;
  }
  
  List<Double> getBonusPercentagesFromProductionPortion(
      Set<String> goalProductionCategories,
      HashMap<String, Double> bonusPercentages,
      HashMap<String, Double> achievedGoalPercentages) {
    List<Double> achievedBonusPercentages = new ArrayList<Double>();
    
    for (String category : goalProductionCategories) {
      Double bonusPercentageItem = 
          (1 - bonusPercentages.get(category))
          * achievedGoalPercentages.get(category);
      achievedBonusPercentages.add(bonusPercentageItem);
    }
    
    return achievedBonusPercentages;
  }
  
  void validateBonusPercentages(List<Double> bonusPercentages) {
    double sum = bonusPercentages.stream().mapToDouble(Double::doubleValue).sum();
    if (sum != 1) {
      throw new IllegalArgumentException(this.invalidBonusPercentageMessage);
    }
  }
  
  void validateproductionGoals(HashMap<String, Double> productionGoals) {
    Collection<Double> goals = productionGoals.values();
    if (goals.contains(Double.valueOf(0))) {
      throw new IllegalArgumentException(this.invalidProductionGoalsMessage);
    }
  }
}
