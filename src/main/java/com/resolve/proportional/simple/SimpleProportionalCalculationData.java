/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.simple;

import com.resolve.proportional.ProportionalCalculationData;
import com.resolve.proportional.ProportionalDefinitionProvider;
import com.resolve.proportional.models.ProportionalCalculationItem;
import com.resolve.proportional.models.ProportionalDefinition;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Simple data provider for proportional calculation.
 *
 * @author Rommel Medina
 *
 */
@Component
public class SimpleProportionalCalculationData implements ProportionalCalculationData {
  
  /**
   * Definitions for proportional calculation.
   */
  private ProportionalDefinition definition;
  
  /**
   * Definition provider service.
   */
  @Autowired
  private ProportionalDefinitionProvider definitionProvider;

  @Override
  public ProportionalDefinition getDefinition(String definitionId) {
    this.definition = this.definitionProvider.get(definitionId);
    return definition;
  }

  @Override
  public HashMap<String, Double> getCategoryDefinitions(ProportionalCalculationItem pci) {
    return this.definition.getCategories().getSpecificCategoryDefinitions(pci.getCategory());
  }

}
