/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.simple;

import com.resolve.proportional.ProportionalDefinitionProvider;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * Definition provider definition for unit Tests.
 *
 * @author Rommel Medina
 *
 */
@TestConfiguration
public class ProportionalDefinitionProviderServiceImplTestContextConfiguration {
  @Bean
  public ProportionalDefinitionProvider proportionalDefinitionProviderService() {
    return new MockProportionalDefinitionProvider();
  }
}
