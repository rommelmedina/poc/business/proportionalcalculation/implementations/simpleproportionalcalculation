package com.resolve.proportional.simple;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.resolve.proportional.ProportionalCalculation;
import com.resolve.proportional.models.ProportionalCalculationItem;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { SimpleProportionalCalculationConfiguration.class })
@SpringBootTest
class SimpleProportionalCalculationApplicationTests {
  
  @Autowired
  ProportionalCalculation pca;

  @Test
  void proportionalCalculation() {
    List<ProportionalCalculationItem> listItems = 
        new ArrayList<ProportionalCalculationItem>();
    ProportionalCalculationItem item = new ProportionalCalculationItem();
    
    // luis Level Cuahu
    item.setId("Luis");
    item.setFixedBase(50000.0);
    item.setBonus(10000.0);
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        new String("Cuahu").trim().toLowerCase());
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_GROUP,
        new String("ResuelveFC").trim().toLowerCase());
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        19.0);
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_GROUP,
        48.0);
    listItems.add(item);
    // martin Level C
    item = new ProportionalCalculationItem();
    item.setId("Martin");
    item.setFixedBase(40000.0);
    item.setBonus(9000.0);
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        new String("C").trim().toLowerCase());
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_GROUP,
        new String("ResuelveFC").trim().toLowerCase());
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        16.0);
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_GROUP,
        48.0);
    listItems.add(item);
    // pedro Level B
    item = new ProportionalCalculationItem();
    item.setId("Pedro");
    item.setFixedBase(30000.0);
    item.setBonus(8000.0);
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        new String("B").trim().toLowerCase());
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_GROUP,
        new String("ResuelveFC").trim().toLowerCase());
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        7.0);
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_GROUP,
        48.0);
    listItems.add(item);
    // juan Level A
    item = new ProportionalCalculationItem();
    item.setId("Juan");
    item.setFixedBase(20000.0);
    item.setBonus(7000.0);
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        new String("A").trim().toLowerCase());
    item.addCategory(
        SalaryConstants.CATEGORY_NAME_GROUP,
        new String("ResuelveFC").trim().toLowerCase());
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_LEVEL,
        6.0);
    item.addProductionQuantity(
        SalaryConstants.CATEGORY_NAME_GROUP,
        48.0);
    listItems.add(item);
    
    this.pca.compute("ResuelveFC", listItems);
    
    // Resuelve FC
    // luis Level Cuahu
    assertEquals(59550.00, listItems.get(0).getResult());
    // martin Level C
    assertEquals(48820.0, listItems.get(1).getResult());
    // pedro Level B
    assertEquals(36640.0, listItems.get(2).getResult());
    // juan Level A
    assertEquals(26860.0, listItems.get(3).getResult());

  }
}